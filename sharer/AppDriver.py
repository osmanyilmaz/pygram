# -*- coding: utf-8 -*-
#!/usr/bin/python3

import platform
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from sharer import dir_path


class AppDriver():

    def get_driver_path(self):
        if platform.system() == 'Darwin':
            return '{0}/../driver/darwin/chromedriver'.format(dir_path)
        else:
            return '{0}/../driver/linux/chromedriver'.format(dir_path)

    def get_driver(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_experimental_option("detach", True)
        chrome_options.add_experimental_option("mobileEmulation", {"deviceName": "iPhone X"})

        return webdriver.Chrome(
            executable_path=self.get_driver_path(),
            desired_capabilities=chrome_options.to_capabilities())
