# -*- coding: utf-8 -*-
#!/usr/bin/python3

import requests
import email.utils as eut
import calendar
import urllib
import json
import datetime
import os
import os.path as op
from multiprocessing import Process
from sharer import dir_path
try:
    from urllib.request import urlretrieve
except ImportError:
    from urllib import urlretrieve


class Downloader(Process):

    json_data = dict()
    time = datetime.datetime.now().strftime("%Y.%m.%d-%H.%M.%S")

    def download(self, media):
        path = '{0}/../uploads/'.format(dir_path)
        image_path = '{0}/../uploads/image/'.format(dir_path)
        video_path = '{0}/../uploads/video/'.format(dir_path)

        posts_file = '{0}/../json/posts.json'.format(dir_path)

        if not op.exists(path):
            os.makedirs(path)

        try:
            image_file = self.download_image(media, image_path)

            if media['type'] == "video":
                video_file = self.download_video(media, video_path)
            else:
                video_file = None

            self.json_data[self.time] = {
                'image': image_file,
                'video': video_file,
                'type': media['type'],
                'description': media['description'],
                'short_description': media['short_description']}

            with open(posts_file, mode='w+') as f:
                f.write(json.dumps(self.json_data, indent=2, ensure_ascii=False))

            return self.json_data
        except:
            return {}

    def download_video(self, media, video_path):
        _, video_extension = os.path.splitext(media['video'])
        video_name = self.time + video_extension
        video_file = video_path + video_name
        r = requests.get(media['video'])

        with open(video_file, 'wb') as f:
            f.write(r.content)

        return video_file

    def download_image(self, media, image_path):
        image_name = self.time + '.jpg'
        image_file = image_path + image_name
        urlretrieve(media['image'], image_file)

        return image_file

