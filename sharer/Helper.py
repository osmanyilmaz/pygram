# -*- coding: utf-8 -*-
#!/usr/bin/python3

import json
import emoji
import random
from sharer import dir_path


class Helper:

    def give_emoji_free_text(self, text):
        all_chars = [str for str in text]
        emoji_list = [c for c in all_chars if c in emoji.UNICODE_EMOJI]
        clean_text = ' '.join([str for str in text.split() if not any(i in str for i in emoji_list)])
        return clean_text

    def source_regex(self, Key=None):
        regex_file = '{0}/../json/regex.json'.format(dir_path)

        json_path_file = open(regex_file, "r")
        regex = json.loads(json_path_file.read())

        if Key is not None:
            return regex[Key]

        return regex

    def read_searches_file(self):
        searches_file = '{0}/../json/search_text.json'.format(dir_path)

        json_path_file = open(searches_file, "r")
        return json.loads(json_path_file.read())

    def random_search_text(self, page_or_keyword='keywords'):
        search_text = self.read_searches_file()
        secure_random = random.SystemRandom()
        return secure_random.choice(search_text[page_or_keyword])

    def scroll_down(self, driver):
        driver.implicitly_wait(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")