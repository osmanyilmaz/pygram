# -*- coding: utf-8 -*-
#!/usr/bin/python3

import os
import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys
from sharer import driver_wait
from sharer import driver_wait_file
from sharer.Helper import Helper
from bs4 import BeautifulSoup


class Pages:

    helper = Helper()

    def login_page(self, driver, username, password):
        try:
            driver.get('https://www.instagram.com/accounts/login/')
            driver.find_element_by_xpath(self.helper.source_regex('username')).send_keys(username)
            driver.find_element_by_xpath(self.helper.source_regex('password')).send_keys(password)
            time.sleep(3)
            driver.find_element_by_xpath(self.helper.source_regex('login_button')).click()
        except:
            print('Error bug in login steps!')

    def page_choose_app_info(self, driver):
        try:
            detected = driver_wait(driver, self.helper.source_regex('choose_app'))
            detected.click()
        except TimeoutException:
            print('instagram app info timeout exception')
        except NoSuchElementException:
            print('error with instagram app detected')

    def page_save_your_login_info(self, driver):
        try:
            if driver.page_source.find("We can save your login info on this browser"):
                detected = driver_wait(driver, self.helper.source_regex('choose_ap'), None, 15)
                detected.click()
            else:
                print('not found save your login info in page source')
        except TimeoutException:
            print('save your login app info timeout exception')
        except:
            print('error with save your login info detected')

    def page_search_by_keywords(self, driver):
        try:
            link = driver_wait(driver, self.helper.source_regex('search_link'))
            link.click()

            media = driver_wait(driver, self.helper.source_regex('first_element'))
            media_text = self.helper.give_emoji_free_text(media.text)
            media_file = driver_wait(driver, self.helper.source_regex('media'))
            media_file.click()

            print(media_text)
        except:
            print('page search by keywords not work!')

    # def post_share(self, driver, media):
        # driver.get('https://www.instagram.com/')
        # image = os.path.abspath(media['image'])
        #
        # share_link = driver_wait_file(driver, self.helper.source_regex('share_link'))
        #
        # actions = ActionChains(driver)
        # driver.implicitly_wait(3)
        # actions.move_to_element(share_link)
        # actions.click()
        # actions.send_keys(image)
        # actions.perform()
        # actions.context_click(Keys.ESCAPE)

    def fetch_media(self, driver):
        try:
            driver.get('https://www.instagram.com/{0}'.format(self.helper.random_search_text('pages')))
            driver_wait(driver, self.helper.source_regex('first_media')).click()
            driver.get(driver.current_url)

            media = driver_wait(driver, self.helper.source_regex('first_element_text'))
            description = self.helper.give_emoji_free_text(media.text)

            soup = BeautifulSoup(driver.page_source, 'html.parser')
            og_type = soup.find('meta', {'property': 'og:type'})
            og_image = soup.find('meta', {'property': 'og:image'})
            og_description = soup.find('meta', {'property': 'og:description'})

            if og_type['content'] == "video":
                og_video = soup.find('meta', {'property': 'og:video'})
                og_video_file = og_video['content']
            else:
                og_video_file = None

            return {
                'type': og_type['content'],
                'video': og_video_file,
                'image': og_image['content'],
                'short_description': og_description['content'],
                'description': description
            }
        except TimeoutException:
            print('Timeout exception in fetch media method!')
        except NoSuchElementException:
            print('No such element exception in fetch media method!')
        except:
            print('Fetch media exception!')

        return {
            'type': '',
            'video': '',
            'image': '',
            'short_description': '',
            'description': ''
        }

    def scroll_page(self, driver):
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(0.05)
        driver.execute_script("window.scrollTo(0, 0);")
        time.sleep(0.05)

    def get_scroll_count(self, count):
        return (int(count) - 12) / 12 + 1
