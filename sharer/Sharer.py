# -*- coding: utf-8 -*-
#!/usr/bin/python3

import time
from sharer.AppDriver import AppDriver
from sharer.Pages import Pages
from sharer.Helper import Helper
from sharer.Downloader import Downloader
from sharer import dir_path
import json

class Sharer:

    def share(self, username, password):
        print('Starting...')
        pages = Pages()
        downloader = Downloader()

        print('Access driver...')
        driver = AppDriver().get_driver()
        driver.set_window_size(400, 940)

        print('Connection login page...')
        pages.login_page(driver, username, password)
        time.sleep(2)
        pages.page_choose_app_info(driver)
        pages.page_save_your_login_info(driver)

        print('Fetch media...')
        media = pages.fetch_media(driver)
        print('Downloading...')
        downloader.download(media)
        print('Download completed!')


        # driver.get('https://www.instagram.com/explore/tags/{0}/'.format(self.helper.random_search_text('pages')))
        # pages.page_search_by_keywords(driver)
