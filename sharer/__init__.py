# -*- coding: utf-8 -*-
#!/usr/bin/python3

# set a globally helpers and variables

import os
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


dir_path = os.path.dirname(os.path.realpath(__file__))
directory = '{0}/../uploads/'.format(dir_path)


def driver_wait(driver, xpath, selector="xPath", timeout=10):
    if selector == "css_selector":
        return WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
    else:
        return WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((By.XPATH, xpath)))

def driver_wait_file(driver, xpath, selector="xPath", timeout=10):
    return WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, xpath)))
